/*
================================================================================
 * Nom: PWM
 * Fitxer: PWM.h
 * Funcionalitat: Driver dels PWM del MCU pel control dels motors.
 * Autor: Albert Lloveras Carbonell (alloveras@salleurl.edu)
 * Última Revisió: 28/01/2015
================================================================================
*/

#ifndef _PWM_H_
#define _PWM_H_

#include <xc.h>
#include "UART.h"
#include "T_Timers.h"

//Pre: Cap
//Post: Inicialitza els tres PWM del MCU.
void PWM_Init(void);

//Pre: Cap
//Post: Assigna el valor de duty al PWM indicat per paràmetre.
void PWM_SetSpeed(unsigned char nPWM,unsigned char nDuty);

//Pre: Cap
//Post: Cap
void PWM_StatusMachine(void);

#endif