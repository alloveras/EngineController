/*
================================================================================
 * Nom: UART
 * Fitxer: UART.h
 * Funcionalitat: Driver de la UART del MCU.
 * Autor: Albert Lloveras Carbonell (alloveras@salleurl.edu)
 * Última Revisió: 29/01/2015
================================================================================
*/

#include "UART.h"

/*
================================================================================
                           CONSTANTS PRIVADES
================================================================================
*/

#define MAX_QUEUE 16

/*
================================================================================
                              TIPUS PRIVATS
================================================================================
*/

typedef struct{
    unsigned char nStart;
    unsigned char nEnd;
    unsigned char nSize;
    char aQueue[MAX_QUEUE];
}UART_Queue;

/*
================================================================================
                           VARIABLES PRIVADES
================================================================================
*/

unsigned char nEstat = 0;
UART_Queue TXQueue;
UART_Queue RXQueue;

/*
================================================================================
                    PROCEDIMENTS I FUNCIONS PRIVADES
================================================================================
*/

//Pre: S'ha d'haver comprovat que hi ha com a mínim un byte a la cua a la qual 
//es vol realitzar el Pop.
//Post: Desencua el byte de la cua pertinent i retorna el valor corresponent a
//aquest. IMPORTANT: La lectura és DESTRUCTIVA.
static char Pop(UART_Queue *pQueue){
    char cReturn = ' ';
    if(pQueue->nSize > 0){
        cReturn = pQueue->aQueue[pQueue->nStart];
        pQueue->nStart++;
        if(pQueue->nStart == MAX_QUEUE) pQueue->nStart = 0;
        pQueue->nSize--;
    }
    return cReturn;
}


//Pre: S'ha d'haver comprovat prèviament que la cua en qüestió té espai per
//emmagatzemar el nou byte.
//Post: Encua el byte rebut com a paràmetre a la cua de sortida per a poder ser
//enviat l'abans possible.
static void Push(UART_Queue *pQueue,char byte){
    if(pQueue->nSize == MAX_QUEUE){
        Pop(pQueue);
    }
    pQueue->aQueue[pQueue->nEnd] = byte;
    pQueue->nEnd++;
    if(pQueue->nEnd == MAX_QUEUE) pQueue->nEnd = 0;
    pQueue->nSize++;
}

/*
================================================================================
                       PROCEDIMENTS I FUNCIONS PÚBLICS
================================================================================
*/


//Pre: Cap
//Post: Inicialitza la UART del MCU configurant-la amb un baudrate de 9600, amb
//una transmissió de 8 bits i amb un stop-bit
void UART_Init(void){

    //Configurem els pins de la UART (Datasheet dixit)
    TRISAbits.TRISA3 = 1;
    TRISAbits.TRISA2 = 1;

    TXQueue.nEnd = 0;
    TXQueue.nStart = 0;
    TXQueue.nSize = 0;

    RXQueue.nSize = 0;
    RXQueue.nStart = 0;
    RXQueue.nEnd = 0;

    //UART Configuration
    TXSTA = 0b00100010;
    RCSTA = 0b10010000;
    BAUDCON = 0b00000010;
    SPBRG = 12;

    PIE1bits.RCIE = 1;
    IPR1bits.RCIP = 1;
    PIR1bits.RCIF = 0;

    //Inicialitzem la màquina d'estats
    nEstat = 0;



}

//Pre: Cap
//Post: Cap
void UART_StatusMachine(void){
    switch(nEstat){
        case 0:
            if(TXQueue.nSize > 0){
                nEstat++;
            }
            break;
        case 1:
            if(TXSTAbits.TRMT == 1){
                nEstat++;
            }
            break;
        case 2:
            TXREG = Pop(&TXQueue);
            nEstat = 0;
            break;
    }
}

//Pre: S'ha d'haver comprovat prèviament que hi ha espai al buffer de sortida de
//la UART.
//Post: Encua el byte rebut per paràmetre a la cua de sortida de la UART per ser
//enviat el més aviat possible.
void UART_WriteByte(char byte){
    if(TXQueue.nSize < MAX_QUEUE){
        Push(&TXQueue,byte);
    }
}

//Pre: S'ha d'haver comprovat que la cua d'entrada de dades de la UART té com a
//mínim un byte.
//Post: Retorna el byte que es troba a la primera posició de la cua d'entrada de
//la UART. IMPORTANT: La lectura és DESTRUCTIVA.
char UART_ReadByte(void){
    char cReturn = ' ';
    if(RXQueue.nSize > 0){
        cReturn = Pop(&RXQueue);
    }
    return cReturn;
}

//Pre: Cap
//Post: Retorna 1 (CERT) si la cua de sortida no té lloc per més caràcters. En
//qualsevol altre cas retorna 0 (FALS).
unsigned char UART_OutputQueueFull(void){
    return TXQueue.nSize == MAX_QUEUE;
}

//Pre: Cap
//Post: Retorna 1 (CERT) si la cua d'entrada de la UART conté un o més caràcters
//En qualsevol altre cas, retorna 0 (FALS).
unsigned char UART_InputQueueEmpty(void){
    return RXQueue.nSize == 0;
}

//Pre: Cap
//Post: Cap
void UART_RSI(void){
    char cData;
    PIR1bits.RC1IF = 0;
    if (RCSTAbits.OERR == 1) RCSTAbits.OERR = 0;
    cData = RCREG;
    Push(&RXQueue,cData);
}
