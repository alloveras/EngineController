/*
================================================================================
 * Nom: PWM
 * Fitxer: PWM.c
 * Funcionalitat: Driver dels PWM del MCU pel control dels motors.
 * Autor: Albert Lloveras Carbonell (alloveras@salleurl.edu)
 * Última Revisió: 28/01/2015
================================================================================
*/

#include "PWM.h"

#define START_DELAY 5000 //Temps d'espera driver dels motors (En milisegons!)
#define STOP_ENGINES 0x00
#define CHANGE_SPEED 0x01
#define ACK 0x02

static char nTimer = -1;
static unsigned char nEstat = 0;
static char cReceived = ' ';
static char nMotor = 0;

//Pre: Cap
//Post: Inicialitza els tres PWM del MCU.
void PWM_Init(void){

    nTimer = T_Timers_GetTimer();

    //Si no tenim timer més val que parem el trasto
    if(nTimer == -1) return;

    /*
    El MCU va a 8MHz i volem obtenir una freqüència del PWM de 500Hz. Això
    comporta un període de 2ms. Per fer-ho, haurem de realitzar els següents
    càlculs:

    TPWM    = Període del PWM
    PTPER   = Registre de 12 bits (PTPERL i PTPERH) que ajusta el període dels
    PWM.
    PTMRPS  = Pre-scaler que es pot utilitzar en cas de necessitar períodes
    no assolibles amb la freqüència de rellotge del MCU.

           (PTPER+1)*PTMRPS     (3999+1) * 1
    TPWM = ----------------  =  ------------ = 0.002s
                Fosc/4           8000000/4

    Si fem els càlculs veurem que el valor de PTPER és de 3999 per tal d'obtenir
    el període de 2ms desitjat.

    Freqüència = 1/TPWM = 500Hz

    */

    //Registres de configuració dels PWM
    //Posem Postscale 1:1, Fosc/4 i Prescale 1:1. El mode d'operació és
    //Free Running. Posem el timer en mode de contatge UP.

    PTCON0  = 0x00;
    PTCON1  = 0x80;


    //Fixem els pins senars com a sortides de PWM i com que són dependents sabem
    //que PWM0 (inactiu) PWM1 (actiu), PWM2 (inactiu) PWM3 (actiu) i PWM4
    //(inactiu) i PWM5 (actiu).
    PWMCON0 = 0x77;
    PWMCON1 = 0x09;
    PWMCON0bits.PWMEN = 4;

    //Fixem el període als registres PTPERH i PTPERL (Valor calculat: 3999)
    PTPERH = 0x0F;
    PTPERL = 0x9F;

    //Deixem els PWM a 50%
    PDC0H = 0x20;
    PDC0L = 0x00;
    PDC1H = 0x20;
    PDC1L = 0x20;
    PDC2H = 0x20;
    PDC2L = 0x20;

    //Resetegem el timer
    T_Timers_ResetTimer(nTimer);

    //Inicialitzem la màquina d'estats
    nEstat = 0;

}

void PWM_StatusMachine(void){
    switch(nEstat){
        case 0:
            if(T_Timers_GetValue(nTimer) >= START_DELAY){
                nEstat++;
            }
            break;
        case 1:
            if(UART_InputQueueEmpty() == 0){
                cReceived = UART_ReadByte();
                nEstat++;
            }
            break;
        case 2:
            if(cReceived == STOP_ENGINES){
                PWM_SetSpeed(0,0);
                PWM_SetSpeed(1,0);
                PWM_SetSpeed(2,0);
                nEstat = 5;
            }else if(cReceived == CHANGE_SPEED){
                if(UART_InputQueueEmpty() == 0){
                    nMotor = UART_ReadByte();
                    nEstat++;
                }
            }else{
                //Tornem al primer estat (Trama incorrecte)
                nEstat = 1;
            }
            break;
        case 3:
            if(UART_InputQueueEmpty() == 0){
                cReceived = UART_ReadByte();
                nEstat++;
            }
            break;
        case 4:
            PWM_SetSpeed(nMotor,cReceived);
            nEstat++;
            break;
        case 5:
            if(UART_OutputQueueFull() == 0){
                UART_WriteByte(ACK);
                nEstat = 1;
            }
            break;
    }
}

//Pre: nPWM ha de valdre 0, 1 o 2
//Post: Assigna el valor de duty al PWM indicat per paràmetre.
void PWM_SetSpeed(unsigned char nPWM,unsigned char nSpeed){

    unsigned int nValor = 0;

    nValor = (unsigned int) (82*nSpeed);
    if(nValor > 8191) nValor = 8191;

    switch(nPWM){

        case 0:
            PDC0H = 0x20 | (nValor >> 8 & 0x1F);
            PDC0L = nValor;
            break;
        case 1:
            PDC1H = 0x20 | (nValor >> 8 & 0x1F);
            PDC1L = nValor;
            break;
        case 2:
            PDC2H = 0x20 | (nValor >> 8 & 0x1F);
            PDC2L = nValor;
            break;

    }


}
