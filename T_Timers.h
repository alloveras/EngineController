/*
================================================================================
 * Nom: TAD TIMERS
 * Fitxer: T_Timers.h
 * Funcionalitat: Gestor de Timers de l'LS Maker
 * Autor: Albert Lloveras Carbonell (alloveras@salleurl.edu)
 * Última Revisió: 26/01/2015
================================================================================
*/

#ifndef _T_TIMERS_H_
#define _T_TIMERS_H_

//Include del MCU
#include <xc.h>

/*
================================================================================
                             FUNCIONS I PROCEDIMENTS
================================================================================
*/

//Pre: Cap
//Post: Inicialitza el TAD Timer perquè proveeixi a l'usuari amb MAX_TIMERS i
//pre estableix que cada pas d'aquests timers serà de 1ms. Paral·lelament,
//configura la interupció d'aquest Timer com a interrupció de màxima prioritat,
//ja que NINGU pot retrassar el pas del temps.
void T_Timers_Init(void);

//Pre: Només es pot cridar dins de rutines d'inicialització, sinó encallarà els
//motors coperatius.
//Post : Retorna un handler que identifica el Timer que se'ns ha associat (el
//valor retornat val entre 0 i 127. Si el valor retornat és -1 llavors és senyal
//de que no hi havia timers disponibles al sistema.
char T_Timers_GetTimer(void);

//Pre : nHandler ha de ser un valor de Timer vàlid (0 - 127)
//Post : Reseteja el timer identificat per nHandler.
void T_Timers_ResetTimer  (char nHandler);

//Pre : nHandler ha de ser un valor de Timer vàlid (0 - 127)
//Post: Retorna la quantitat de passos que ha fet el timer identificat per el
//nHandler des de l'últim reset del mateix.
unsigned int T_Timers_GetValue(char nHandler);

//Pre: nHandler ha de tenir un valor entre 0 i 127.
//Post: Allibera el timer que s'identifica amb el valor nHandler.
void T_Timers_FreeTimer(char nHandler);

//Pre: Cap
//Post: Cap
void T_Timers_RSI(void);


#endif
