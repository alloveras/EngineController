/*
================================================================================
 * Nom: TAD TIMERS
 * Fitxer: T_TIMERS.c
 * Funcionalitat: Gestor de Timers de l'LS Maker
 * Autor: Albert Lloveras Carbonell (alloveras@salleurl.edu)
 * Última Revisió: 26/01/2015
================================================================================
*/

//Includes de les capçaleres
#include "T_Timers.h"

/*
================================================================================
                            CONSTANTS PRIVADES
================================================================================
*/

#define MAX_TIMERS 32      //Nombre màxim de timers simultanis dincs del LSMaker
#define MAX_TIMER_VALUE 30000 //Nombre maxim dels tics universals (INT Overflow)
#define TRUE 1                  //Definició de CERT
#define FALSE 0                 //Definició de FALS

/*
================================================================================
                         DEFINICIÓ DE TIPUS PRIVATS
================================================================================
*/
struct{
    unsigned int nInitialValue;
    unsigned char bIsBusy;
}s_Timers[MAX_TIMERS];

/*
================================================================================
                             VARIABLES PRIVADES
================================================================================
*/

static unsigned int nGlobalTimer;       //Comptador de tics universals

/*
================================================================================
 RUTINA DE SERVEI D'INTERRUPCIÓ TIMER1 (MAXIMA PRIORITAT)
================================================================================
*/


//Pre : Cap
//Post: Cap
void T_Timers_RSI(void){

    unsigned char nCounter = 0;
    
    //Incrementem Tics Universals
    nGlobalTimer++;

    //Comprovem si la variable nTicsUniversals esta aprop de l'overflow
    if (nGlobalTimer >= MAX_TIMER_VALUE) {

        //Si es dona el cas, abans de que succeeixi l'overflow, els tirem
        //tots avall
        for (nCounter = 0; nCounter < MAX_TIMERS ; nCounter++){
             if (s_Timers[nCounter].bIsBusy == TRUE){
                 s_Timers[nCounter].nInitialValue -= nGlobalTimer;
             }
        }
        nGlobalTimer = 0;
    }

    //Resetegem Timer0
    TMR0H = 0xF8;
    TMR0L = 0x2F;
    INTCONbits.T0IF = 0;


}

//Pre: Cap
//Post: Inicialitza el TAD Timer perquè proveeixi a l'usuari amb MAX_TIMERS i
//pre estableix que cada pas d'aquests timers serà de 1ms. Paral·lelament,
//configura la interupció d'aquest Timer com a interrupció de màxima prioritat,
//ja que NINGU pot retrassar el pas del temps.
void T_Timers_Init(void) {

    //Declarem variables
    unsigned char nCounter;

    //Configurem el Timer0 del MCU
    T0CONbits.TMR0ON = 1;
    T0CONbits.T016BIT = 0;
    T0CONbits.T0CS = 0;
    T0CONbits.T0SE = 1;
    T0CONbits.PSA = 1;
    T0CONbits.T0PS = 0;
    TMR0H = 0xF8;
    TMR0L = 0x2F;

    //Configurem la interrupció del Timer0 (Prioritat Alta)
    INTCONbits.T0IE = 1;
    INTCON2bits.TMR0IP = 1;
    INTCONbits.T0IF = 0;

    //Alliberem tots els Timers
    for(nCounter = 0; nCounter < MAX_TIMERS ; nCounter++){
        s_Timers[nCounter].bIsBusy = FALSE;
    }

    //Resetegem el valor dels Tics Universals
    nGlobalTimer = 0;

}

//Pre: Només es pot cridar dins de rutines d'inicialització, sinó encallarà els
//motors coperatius.
//Post : Retorna un handler que identifica el Timer que se'ns ha associat (el
//valor retornat val entre 0 i 127. Si el valor retornat és -1 llavors és senyal
//de que no hi havia timers disponibles al sistema.
char T_Timers_GetTimer(void) {

    //Declaració de variables
    unsigned char nCounter = 0;

    //Recorrem l'array de Timers disponibles cercant-ne un de lliure
    for(nCounter = 0 ; nCounter < MAX_TIMERS ; nCounter++){
        //Si està lliure el posem a ocupat i retornem el handler del Timer
        if(s_Timers[nCounter].bIsBusy == FALSE){
            s_Timers[nCounter].bIsBusy = TRUE;
            return nCounter;
        }
    }

    //Si no hem trobat cap Timer lliure el handler val -1
    return -1;
}


//Pre : nHandler ha de ser un valor de Timer vàlid (0 - 127)
//Post : Reseteja el timer identificat per nHandler.
void T_Timers_ResetTimer (char nHandler) {
    if(nHandler >= 0 && nHandler < MAX_TIMERS){
        s_Timers[(unsigned char)nHandler].nInitialValue = nGlobalTimer;
    }
}


//Pre : nHandler ha de ser un valor de Timer vàlid (0 - 127)
//Post: Retorna la quantitat de passos que ha fet el timer identificat per el
//nHandler des de l'últim reset del mateix.
unsigned int T_Timers_GetValue(char nHandler) {
    //Retornem el valor de la resta entre nTicsUniversals i els tics inicials
    //del timer en qüestió
    if(nHandler >= 0 && nHandler < MAX_TIMERS){
        return (nGlobalTimer-(s_Timers[(unsigned char)nHandler].nInitialValue));
    }
    return 0;
}

//Pre: nHandler ha de tenir un valor entre 0 i 127.
//Post: Allibera el timer que s'identifica amb el valor nHandler.
void T_Timers_FreeTimer(char nHandler) {
    if(nHandler >= 0 && nHandler < MAX_TIMERS){
        s_Timers[(unsigned char)nHandler].bIsBusy = FALSE;
    }
}


