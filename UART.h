/*
================================================================================
 * Nom: UART
 * Fitxer: UART.h
 * Funcionalitat: Driver de la UART del MCU.
 * Autor: Albert Lloveras Carbonell (alloveras@salleurl.edu)
 * Última Revisió: 29/01/2015
================================================================================
*/

#ifndef _UART_H_
#define _UART_H_

#include <xc.h>

//Pre: Cap
//Post: Inicialitza la UART del MCU configurant-la amb un baudrate de 9600, amb
//una transmissió de 8 bits i amb un stop-bit
void UART_Init(void);

//Pre: Cap
//Post: Cap
void UART_StatusMachine(void);

//Pre: S'ha d'haver comprovat prèviament que hi ha espai al buffer de sortida de
//la UART.
//Post: Encua el byte rebut per paràmetre a la cua de sortida de la UART per ser
//enviat el més aviat possible.
void UART_WriteByte(char byte);

//Pre: S'ha d'haver comprovat que la cua d'entrada de dades de la UART té com a
//mínim un byte.
//Post: Retorna el byte que es troba a la primera posició de la cua d'entrada de
//la UART. IMPORTANT: La lectura és DESTRUCTIVA.
char UART_ReadByte(void);

//Pre: Cap
//Post: Retorna 1 (CERT) si la cua de sortida no té lloc per més caràcters. En
//qualsevol altre cas retorna 0 (FALS).
unsigned char UART_OutputQueueFull(void);

//Pre: Cap
//Post: Retorna 1 (CERT) si la cua d'entrada de la UART conté un o més caràcters
//En qualsevol altre cas, retorna 0 (FALS).
unsigned char UART_InputQueueEmpty(void);

//Pre: Cap
//Post: Cap
void UART_RSI(void);

#endif
