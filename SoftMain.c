#include <xc.h>
#include "PWM.h"
#include "UART.h"
#include "T_Timers.h"

#pragma config OSC = INTIO2
#pragma config WDT = OFF
#pragma config BOR = OFF

/*
  ==============================================================================
                              INICIALITZACIÓ MCU
  ==============================================================================
*/

void MCU_Init(void){
    //Fixem la freqüència del MCU a 8MHz
    OSCCON = 0x70;

    //Configurem interrupcions
    RCONbits.IPEN = 1;
    INTCONbits.GIE = 1;
    INTCONbits.PEIE_GIEL = 1;

}

/*
  ==============================================================================
                                INTERRUPCIONS
  ==============================================================================
*/


void interrupt High_RSI(void){

    //Comprovem si la RSI la ha disparat la UART
    if(PIR1bits.RC1IF == 1){
        UART_RSI();
    }

    if(INTCONbits.TMR0IF == 1){
        T_Timers_RSI();
    }
    
}

void interrupt low_priority Low_RSI(void){

}


 /*
  ==============================================================================
                                PROCEDIMENT PRINCIPAL
  ==============================================================================
  */


int main(void){

    char nTimer = -1;
    unsigned int vel = 0x2000;

    MCU_Init();
    T_Timers_Init();
    PWM_Init();
    //UART_Init();

    nTimer = T_Timers_GetTimer();
    T_Timers_ResetTimer(nTimer);

    while(T_Timers_GetValue(nTimer) < 5000);

    T_Timers_ResetTimer(nTimer);
    //Comença la festa
    while(1){

        if(T_Timers_GetValue(nTimer) >= 1){
            if(vel < 0x3DFF){
                vel++;
                PDC0H = (unsigned char)(vel >> 8) & 0x00FF;
                PDC0L = (unsigned char) vel & 0x00FF;
                PDC1H = (unsigned char)(vel >> 8) & 0x00FF;
                PDC1L = (unsigned char) vel & 0x00FF;
                PDC2H = (unsigned char)(vel >> 8) & 0x00FF;
                PDC2L = (unsigned char) vel & 0x00FF;
            }
             
            T_Timers_ResetTimer(nTimer);
        }
        //UART_StatusMachine();
        //PWM_StatusMachine();
    }

    return 0;
}